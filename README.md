# Argo CD - Declarative Continuous Delivery for Kubernetes

## What is Argo CD?

Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes.

## Why Argo CD?

1. Application definitions, configurations, and environments should be declarative and version controlled.
1. Application deployment and lifecycle management should be automated, auditable, and easy to understand.

## Documentation

To learn more about Argo CD [go to the complete documentation](https://argoproj.github.io/argo-cd/).
Check live demo at https://cd.apps.argoproj.io/.

## Notes

You must mount an `ssh_known_hosts` file when using argocli to pull git sources over ssh.
```
docker run --rm -v $(PWD)/ssh_known_hosts:/etc/ssh/ssh_known_hosts -it argoproj/argocli [cmd]
```
