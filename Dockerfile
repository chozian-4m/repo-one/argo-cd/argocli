ARG BASE_REGISTRY=registry1.dsop.io
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM argoproj/argocli:v3.3.3 as argocli

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

ENV HOME=/home/argocd \
    USER=argocd

RUN groupadd -g 1000 argocd && \
    useradd -r -u 1000 -m -s /sbin/nologin -g argocd argocd && \
    chown argocd:0 ${HOME} && \
    chmod g=u ${HOME} && \
    dnf update -y && \
    dnf clean all && \
    rm -rf /var/cache/dnf

COPY --from=argocli /bin/argo /bin/argo

USER argocd
WORKDIR ${HOME}

HEALTHCHECK NONE

ENTRYPOINT [ "argo" ]
